#include <QGLWidget>
#include <GL/glu.h>
#include <QWidget>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QKeyEvent>
#include <QPointF>
#pragma once
#include <QTimer>
#include "Ball.hpp"
#include "PhysicsModule.hpp"
#include "Floor.hpp"

//Material properties
typedef struct materialStruct
{
	float specular[4];
	float diffuse[4];
	float ambient[4];
	float shininess;

} materialStruct;

class BallSimWidget: public QGLWidget
{

	Q_OBJECT

	public:
		BallSimWidget(QWidget* parent);
		~BallSimWidget();

		PhysicsModule* getPhysics() {return physics;};
		Ball* getActiveBall() {return active;};

	public slots:

		void resetWorld();
		void updatePhysics();
		void playSimulation();
		void pauseSimulation();
		void resetSimulation();

		void addBall();
		void deleteActiveBall();
		void setActiveBall(Ball* b);

		void setActiveRadius(int r);
		void setActiveMass(int m);

		void setActiveX(int x);
		void setActiveY(int y);
		void setActiveZ(int z);

		void setActiveVX(int vx);
		void setActiveVY(int vy);
		void setActiveVZ(int vz);

		void setFps(int fps);

		void setFloorSize(int size);
		void setFloorHeight(int h);



	signals:

		void publishActiveRadius(float r);
		void publishActiveMass(float m);

		void publishActiveX(float x);
		void publishActiveY(float y);
		void publishActiveZ(float z);

		void publishActiveVX(float vx);
		void publishActiveVY(float vy);
		void publishActiveVZ(float vz);

		void noActiveBall();
		void newActiveBall();

	protected:

		//OpenGL event handlers
		void initializeGL();
		void resizeGL(int width, int height);
		void paintGL();

		//Mouse event handlers
		virtual void mousePressEvent(QMouseEvent* event);
		virtual void mouseMoveEvent(QMouseEvent* event);
		virtual void mouseReleaseEvent(QMouseEvent* event);
		virtual void wheelEvent(QWheelEvent* event);

		//Keyboard event handler
		virtual void keyReleaseEvent(QKeyEvent* event);

	private:

		//Physics handler
		PhysicsModule* physics;

		//Use QTimer to update physics sim
		QTimer* timer;

		//Ball whose settings we are interested in
		int activeIndex;
		Ball* active;

		//Constant parameters that affect world
		const float bgColourR = 0.5;
		const float bgColourG = 0.5;
		const float bgColourB = 0.5;

		float camPosX = 0.0;
		float camPosY = 2.0;
		float camPosZ = 8.5;

		const float lightPosX = -5.0;
		const float lightPosY = 20.0;
		const float lightPosZ = 20.0;

		const int floorSize = 20.0;
		const float floorHeight = 0.0;

		const float rotateFactor = 0.2;
		const float translateFactor = 0.1;
		const float translateZFactor = 0.005;

		//World transformation parameters
		float worldRotX;
		float worldRotY;

		//Tracks mouse location for world rotations
		QPointF mousePos;

		//Methods for drawing shapes
		void drawFloor(int size, float height);

		void disableActiveBall();
};