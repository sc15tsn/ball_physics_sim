#include "MainWidget.hpp"

MainWidget::MainWidget(QWidget* parent): QWidget(parent)
{
	//Create widget elements
	display = new DisplayWidget(this);
	bSettings = new BallSettingsWidget(this);
	wSettings = new WorldSettingsWidget(this);

	//Arrange widget elements
	settingsLayout = new QVBoxLayout();

	settingsLayout -> addWidget(bSettings);
	settingsLayout -> addSpacing(20);
	settingsLayout -> addWidget(wSettings);
	settingsLayout -> addSpacing(200);

	mainLayout = new QHBoxLayout();

	mainLayout -> addWidget(display);
	mainLayout -> addLayout(settingsLayout);

	//Set main layout
	this -> setLayout(mainLayout);

	//Set up connections
	
	connect(this -> display -> getSim(), SIGNAL(publishActiveRadius(float)), this -> bSettings, SLOT(setRadiusSlider(float)));
	connect(this -> bSettings -> getRadiusSlider(), SIGNAL(valueChanged(int)), this -> display -> getSim(), SLOT(setActiveRadius(int)));

	connect(this -> display -> getSim(), SIGNAL(publishActiveMass(float)), this -> bSettings, SLOT(setMassSlider(float)));
	connect(this -> bSettings -> getMassSlider(), SIGNAL(valueChanged(int)), this -> display -> getSim(), SLOT(setActiveMass(int)));

	connect(this -> display -> getSim(), SIGNAL(publishActiveX(float)), this -> bSettings, SLOT(setXSlider(float)));
	connect(this -> bSettings -> getXSlider(), SIGNAL(valueChanged(int)), this -> display -> getSim(), SLOT(setActiveX(int)));

	connect(this -> display -> getSim(), SIGNAL(publishActiveY(float)), this -> bSettings, SLOT(setYSlider(float)));
	connect(this -> bSettings -> getYSlider(), SIGNAL(valueChanged(int)), this -> display -> getSim(), SLOT(setActiveY(int)));

	connect(this -> display -> getSim(), SIGNAL(publishActiveZ(float)), this -> bSettings, SLOT(setZSlider(float)));
	connect(this -> bSettings -> getZSlider(), SIGNAL(valueChanged(int)), this -> display -> getSim(), SLOT(setActiveZ(int)));

	connect(this -> display -> getSim(), SIGNAL(publishActiveVX(float)), this -> bSettings, SLOT(setVXSlider(float)));
	connect(this -> bSettings -> getVXSlider(), SIGNAL(valueChanged(int)), this -> display -> getSim(), SLOT(setActiveVX(int)));

	connect(this -> display -> getSim(), SIGNAL(publishActiveVY(float)), this -> bSettings, SLOT(setVYSlider(float)));
	connect(this -> bSettings -> getVYSlider(), SIGNAL(valueChanged(int)), this -> display -> getSim(), SLOT(setActiveVY(int)));

	connect(this -> display -> getSim(), SIGNAL(publishActiveVZ(float)), this -> bSettings, SLOT(setVZSlider(float)));
	connect(this -> bSettings -> getVZSlider(), SIGNAL(valueChanged(int)), this -> display -> getSim(), SLOT(setActiveVZ(int)));
	
	connect(this -> wSettings -> getFpsSlider(), SIGNAL(valueChanged(int)), this -> display -> getSim(), SLOT(setFps(int)));

	connect(this -> wSettings -> getHeightSlider(), SIGNAL(valueChanged(int)), this -> display -> getSim(), SLOT(setFloorHeight(int)));
	connect(this -> wSettings -> getSizeSlider(), SIGNAL(valueChanged(int)), this -> display -> getSim(), SLOT(setFloorSize(int)));

	connect(this -> display -> getPlayButton(), SIGNAL(clicked()), this -> bSettings, SLOT(disable()));
	connect(this -> display -> getPauseButton(), SIGNAL(clicked()), this -> bSettings, SLOT(disable()));
	connect(this -> display, SIGNAL(safeToChangeBallSettings()), this -> bSettings, SLOT(enable()));

	connect(this -> display -> getPlayButton(), SIGNAL(clicked()), this -> wSettings, SLOT(disable()));
	connect(this -> display -> getPauseButton(), SIGNAL(clicked()), this -> wSettings, SLOT(enable()));
	connect(this -> display -> getRewindButton(), SIGNAL(clicked()), this -> wSettings, SLOT(enable()));

	connect(this -> display -> getSim(), SIGNAL(noActiveBall()), this -> bSettings, SLOT(disable()));
	connect(this -> display -> getSim(), SIGNAL(newActiveBall()), this -> bSettings, SLOT(enable()));

}

MainWidget::~MainWidget()
{
	//Delete pointers
	delete display;
	delete bSettings;
	delete wSettings;
	delete settingsLayout;
	delete mainLayout;
}