#include "BallSettingsWidget.hpp"
#include <iostream>

BallSettingsWidget::BallSettingsWidget(QWidget* parent): QFrame(parent)
{

	//Create elements
	generalText = new QLabel("General Properties");

	radiusText = new QLabel("R:");
	radius = new QSlider();
	radius -> setOrientation(Qt::Horizontal);
	radius -> setRange(50, 500);
	radiusReset = new QPushButton("Reset");

	massText = new QLabel("M:");
	mass = new QSlider();
	mass -> setOrientation(Qt::Horizontal);
	mass -> setRange(100, 1000);
	massReset = new QPushButton("Reset");

	posText = new QLabel("Initial Position");

	initPosX = new QSlider();
	initPosX -> setRange(-800, 800);
	initPosY = new QSlider();
	initPosY -> setRange(-800, 800);
	initPosZ = new QSlider();
	initPosZ -> setRange(-800, 800);
	initPosX -> setOrientation(Qt::Horizontal);
	initPosY -> setOrientation(Qt::Horizontal);
	initPosZ -> setOrientation(Qt::Horizontal);
	xText = new QLabel("X:");
	yText = new QLabel("Y:");
	zText = new QLabel("Z:");
	initPosReset = new QPushButton("Reset");

	velText = new QLabel("Initial Velocity");

	initVelX = new QSlider();
	initVelX -> setRange(-800, 800);
	initVelY = new QSlider();
	initVelY -> setRange(-800, 800);
	initVelZ = new QSlider();
	initVelZ -> setRange(-800, 800);
	initVelX -> setOrientation(Qt::Horizontal);
	initVelY -> setOrientation(Qt::Horizontal);
	initVelZ -> setOrientation(Qt::Horizontal);
	vxText = new QLabel("VX:");
	vyText = new QLabel("VY:");
	vzText = new QLabel("VZ:");
	initVelReset = new QPushButton("Reset");

	//Create and arrange layouts
	propLayout = new QGridLayout();
	propLayout -> addWidget(generalText, 0, 0, 1, 2);
	propLayout -> addWidget(radiusText, 1, 0);
	propLayout -> addWidget(radius, 1, 1);
	propLayout -> addWidget(radiusReset, 1, 2);
	propLayout -> addWidget(massText, 2, 0);
	propLayout -> addWidget(mass, 2, 1);
	propLayout -> addWidget(massReset, 2, 2);
	
	posLayout = new QGridLayout();
	posLayout -> addWidget(posText, 0, 0, 1, 2);
	posLayout -> addWidget(xText, 1, 0);
	posLayout -> addWidget(yText, 2, 0);
	posLayout -> addWidget(zText, 3, 0);
	posLayout -> addWidget(initPosX, 1, 1);
	posLayout -> addWidget(initPosY, 2, 1);
	posLayout -> addWidget(initPosZ, 3, 1);
	posLayout -> addWidget(initPosReset, 1, 2);

	velLayout = new QGridLayout();
	velLayout -> addWidget(velText, 0, 0, 1, 2);
	velLayout -> addWidget(vxText, 1, 0);
	velLayout -> addWidget(vyText, 2, 0);
	velLayout -> addWidget(vzText, 3, 0);
	velLayout -> addWidget(initVelX, 1, 1);
	velLayout -> addWidget(initVelY, 2, 1);
	velLayout -> addWidget(initVelZ, 3, 1);
	velLayout -> addWidget(initVelReset, 1, 2);

	mainLayout = new QVBoxLayout();
	mainLayout -> addLayout(propLayout);
	mainLayout -> addLayout(posLayout);
	mainLayout -> addLayout(velLayout);

	//Set main layout
	this -> setLayout(mainLayout);

	//Make connections
	connect(this -> radiusReset, SIGNAL(clicked()), this, SLOT(resetRadiusSlider()));
	connect(this -> massReset, SIGNAL(clicked()), this, SLOT(resetMassSlider()));

	connect(this -> initPosReset, SIGNAL(clicked()), this, SLOT(resetPosSliders()));

	connect(this -> initVelReset, SIGNAL(clicked()), this, SLOT(resetVelSliders()));
}

BallSettingsWidget::~BallSettingsWidget()
{
	delete generalText;

	delete radiusText;
	delete radius;
	delete radiusReset;
	delete propLayout;

	delete massText;
	delete mass;
	delete massReset;

	delete posText;

	delete xText;
	delete yText;
	delete zText;
	delete initPosX;
	delete initPosY;
	delete initPosZ;
	delete initPosReset;
	delete posLayout;

	delete velText;

	delete vxText;
	delete vyText;
	delete vzText;
	delete initVelX;
	delete initVelY;
	delete initVelZ;
	delete initVelReset;
	delete velLayout;

	delete mainLayout;
}

void BallSettingsWidget::setRadiusSlider(float val)
{
	radius -> setValue((int)(val * 100));
}

void BallSettingsWidget::setMassSlider(float val)
{
	mass -> setValue((int)(val * 100));
}

void BallSettingsWidget::setXSlider(float x)
{
	initPosX -> setValue((int)(x * 100));
}

void BallSettingsWidget::setYSlider(float y)
{
	initPosY -> setValue((int)(y * 100));
}

void BallSettingsWidget::setZSlider(float z)
{
	initPosZ -> setValue((int)(z * 100));
}

void BallSettingsWidget::setVXSlider(float vx)
{
	initVelX -> setValue((int)(vx * 100));
}

void BallSettingsWidget::setVYSlider(float vy)
{
	initVelY -> setValue((int)(vy * 100));
}

void BallSettingsWidget::setVZSlider(float vz)
{
	initVelZ -> setValue((int)(vz * 100));
}

void BallSettingsWidget::resetRadiusSlider()
{
	setRadiusSlider(1.0);
}

void BallSettingsWidget::resetMassSlider()
{
	setMassSlider(1.0);
}

void BallSettingsWidget::resetPosSliders()
{
	setXSlider(0.0);
	setYSlider(0.0);
	setZSlider(0.0);
}

void BallSettingsWidget::resetVelSliders()
{
	setVXSlider(0.0);
	setVYSlider(0.0);
	setVZSlider(0.0);
}