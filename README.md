# ball_physics_sim

---
Running on DEC-10
---

1) Add the following QT module
```sh
module add qt/5.3.1
```

2) Navigate to the code directory and compile
```sh
qmake
make
```

3) Run the program
```sh
./ball_physics_sim
```

---
Attribution
---
Repository icon made by <a href="https://www.flaticon.com/authors/those-icons" title="Those Icons">Those Icons</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
