#include "BallMainWindow.hpp"
#include "MainWidget.hpp"
#include <QSize>

BallMainWindow::BallMainWindow(): QMainWindow()
{
	this -> setWindowTitle("Ball Physics Simulation");
}

void BallMainWindow::resizeEvent(QResizeEvent* e)
{
	QSize windowSize = e->size();

	MainWidget* main = (MainWidget*) this -> centralWidget();
	main -> getBallSettings() -> setMaximumWidth(windowSize.width() / 5);
	main -> getWorldSettings() -> setMaximumWidth(windowSize.width() / 5);
}