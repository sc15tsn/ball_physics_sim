#include <GL/glu.h>
#include <GL/glut.h>
#include "Ball.hpp"
#include <QDebug>
#include <iostream>
#include <cmath>

Ball::Ball(float rad, float mass, float x, float y, float z, float vx, float vy, float vz)
{
	r = rad;
	m = mass;

	pos0[0] = x;
	pos0[1] = y;
	pos0[2] = z;

	vel0[0] = vx;
	vel0[1] = vy;
	vel0[2] = vz;

	for(int i = 0; i < 3; i++)
	{
		pos[i] = pos0[i];
		vel[i] = vel0[i];
	}
}

Ball::Ball()
{
	Ball(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
}

void Ball::update(float dt, Floor f, std::vector<Ball*> balls)
{

	floorCollision(f);
	ballCollision(balls);

	for(int i = 0; i < 3; i++)
	{
		//Update position
		pos[i] += vel[i] * dt;
	
		//Update velocity
		vel[i] += acc[i] * dt;
	}

	//Update acceleration
	acc[1] = -9.81;
}

void Ball::draw(float red, float green, float blue)
{
	float colour[4] = {red, green, blue, 0.0};
	glPushMatrix();
	glTranslatef(pos[0], pos[1], pos[2]);
	glMaterialfv(GL_FRONT, GL_AMBIENT, colour);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, colour);
	GLUquadric* sphere = gluNewQuadric();
	gluQuadricDrawStyle(sphere, GLU_FILL);
	gluQuadricNormals(sphere, GLU_SMOOTH);
	gluSphere(sphere, r, 1000, 1000);
	glPopMatrix();
}

void Ball::reset()
{
	for(int i = 0; i < 3; i++)
	{
		pos[i] = pos0[i];
		vel[i] = vel0[i];
	}
}

void Ball::floorCollision(Floor f)
{
	bool ballOutsideFloor = (abs(pos[0]) > ((float)f.getSize() / 2)) || (abs(pos[2]) > ((float)f.getSize() / 2));
	bool ballTouchingFloor = (pos[1] - r) <= f.getHeight();
	
	if(!ballOutsideFloor && ballTouchingFloor)
	{
		vel[1] = vel[1] - (2*vel[1]/((float)m + 25))*25;
	}
}

void Ball::ballCollision(std::vector<Ball*> balls)
{
	//Iterate over balls
	for(Ball* b: balls)
	{
		//If the distance between the balls is >= the sum of the radii, then there is a collision
		float diff[3];
		float a1 = 0;
		float a2 = 0;

		for(int i = 0; i < 3; i++)
		{
			diff[i] = b -> getPos(i) - pos[i];
		}

		float dist = sqrt(diff[0]*diff[0] + diff[1]*diff[1] + diff[2]*diff[2]);
		float n[3];

		for(int i = 0; i < 3; i++)
		{
			n[i] = diff[i]/dist;
		}

		if(dist <= r + b -> getRadius())
		{
			for(int i = 0; i < 3; i++)
			{
				a1 += vel[i] * n[i];
				a2 += b -> getVel(i) * n[i]; 
			}

			for(int i = 0; i < 3; i++)
			{
				vel[i] = vel[i] - (2*(a1 - a2)/((float)m + b -> getMass())) * b -> getMass() * n[i];
				b -> setVel(i,  b -> getVel(i) + (2*(a1 - a2)/((float)m + b -> getMass())) * m * n[i]);
			}
		}

	}
}
