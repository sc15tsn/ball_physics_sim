#include "DisplayWidget.hpp"
#include <iostream>

DisplayWidget::DisplayWidget(QWidget* parent): QWidget(parent)
{
	//Create elements
	sim = new BallSimWidget(this);
	reset = new QPushButton("Reset Camera");
	play = new QPushButton("Play");
	pause = new QPushButton("Pause");
	rewind = new QPushButton("Rewind");
	addBall = new QPushButton("Add Ball");
	deleteBall = new QPushButton("Delete Ball");

	//Arrange elements
	buttonLayout = new QHBoxLayout();
	buttonLayout -> addWidget(reset);
	buttonLayout -> addWidget(play);
	buttonLayout -> addWidget(pause);
	buttonLayout -> addWidget(rewind);
	buttonLayout -> addWidget(addBall);
	buttonLayout -> addWidget(deleteBall);

	mainLayout = new QVBoxLayout();
	mainLayout -> addWidget(sim);
	mainLayout -> addLayout(buttonLayout);

	//Set main layout of this widget
	this -> setLayout(mainLayout);

	//Connect signals to slots
	connect(reset, SIGNAL(clicked()), sim, SLOT(resetWorld()));

	connect(play, SIGNAL(clicked()), this, SLOT(playSlot()));
	connect(pause, SIGNAL(clicked()), this, SLOT(pauseSlot()));
	connect(rewind, SIGNAL(clicked()), this, SLOT(rewindSlot()));

	connect(play, SIGNAL(clicked()), sim, SLOT(playSimulation()));
	connect(pause, SIGNAL(clicked()), sim, SLOT(pauseSimulation()));
	connect(rewind, SIGNAL(clicked()), sim, SLOT(resetSimulation()));

	connect(addBall, SIGNAL(clicked()), sim, SLOT(addBall()));
	connect(deleteBall, SIGNAL(clicked()), sim, SLOT(deleteActiveBall()));
	
	//Initially set simulation to rewound state
	rewindSlot();
}

DisplayWidget::~DisplayWidget()
{
	delete sim;
	delete reset;
	delete play;
	delete pause;
	delete rewind;
	delete buttonLayout;
	delete mainLayout;
}

void DisplayWidget::playSlot()
{
	play -> setEnabled(false);
	pause -> setEnabled(true);
	rewind -> setEnabled(true);
	addBall -> setEnabled(false);
	deleteBall -> setEnabled(false);
	//Other play code
}

void DisplayWidget::pauseSlot()
{
	play -> setEnabled(true);
	pause -> setEnabled(false);
	rewind -> setEnabled(true);
	addBall -> setEnabled(false);
	deleteBall -> setEnabled(true);

	//Other pause code
}

void DisplayWidget::rewindSlot()
{
	play -> setEnabled(true);
	pause -> setEnabled(false);
	rewind -> setEnabled(false);
	addBall -> setEnabled(true);
	deleteBall -> setEnabled(true);
	
	//Other rewind code
	if(sim -> getActiveBall() != NULL)
	{
		emit safeToChangeBallSettings();
	}
}