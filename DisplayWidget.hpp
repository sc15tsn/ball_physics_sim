#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "BallSimWidget.hpp"

class DisplayWidget: public QWidget
{

	Q_OBJECT

	public:
		DisplayWidget(QWidget* parent);
		~DisplayWidget();

		BallSimWidget* getSim() {return sim;};

		QPushButton* getPlayButton() {return play;};
		QPushButton* getPauseButton() {return pause;};
		QPushButton* getRewindButton() {return rewind;};

	public slots:

		void playSlot();
		void pauseSlot();
		void rewindSlot();

	signals:

		void safeToChangeBallSettings();

	private:

		BallSimWidget* sim;
		QPushButton* reset;
		QPushButton* play;
		QPushButton* pause;
		QPushButton* rewind;
		QPushButton* addBall;
		QPushButton* deleteBall;

		QVBoxLayout* mainLayout;
		QHBoxLayout* buttonLayout;


};