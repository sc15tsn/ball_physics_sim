#include <QWidget>
#include <QFrame>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QSlider>
#include <QPushButton>

class WorldSettingsWidget: public QFrame
{
	Q_OBJECT

	public:

		WorldSettingsWidget(QWidget* parent);
		~WorldSettingsWidget();

		QSlider* getFpsSlider() {return fps;};
		QSlider* getSizeSlider() {return size;};
		QSlider* getHeightSlider() {return height;};

	public slots:

		void disable() {setEnabled(false);};
		void enable() {setEnabled(true);};

		void resetFps();
		void resetSize();
		void resetHeight();

	private:

		QLabel* simText;

		QLabel* collisionText;
		QSlider* collision;
		QPushButton* collisionReset;

		QLabel* fpsText;
		QSlider* fps;
		QPushButton* fpsReset;

		QLabel* floorText;

		QLabel* sizeText;
		QSlider* size;
		QPushButton* sizeReset;

		QLabel* heightText;
		QSlider* height;
		QPushButton* heightReset;

		QVBoxLayout* mainLayout;
		QGridLayout* simLayout;
		QGridLayout* floorLayout;
};