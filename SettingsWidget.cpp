#include "BallSettingsWidget.hpp"
#include <iostream>

BallSettingsWidget::BallSettingsWidget(QWidget* parent): QWidget(parent)
{

	//Create elements
	radiusText = new QLabel("R:");
	radius = new QSlider();
	radius -> setOrientation(Qt::Horizontal);
	radius -> setRange(50, 500);
	radius -> setTracking(true);

	radiusReset = new QPushButton("Reset");

	initPosX = new QSlider();
	initPosX -> setRange(-800, 800);
	initPosY = new QSlider();
	initPosY -> setRange(-800, 800);
	initPosZ = new QSlider();
	initPosZ -> setRange(-800, 800);
	initPosX -> setOrientation(Qt::Horizontal);
	initPosY -> setOrientation(Qt::Horizontal);
	initPosZ -> setOrientation(Qt::Horizontal);
	xText = new QLabel("X:");
	yText = new QLabel("Y:");
	zText = new QLabel("Z:");
	initPosReset = new QPushButton("Reset");

	initVelX = new QSlider();
	initVelX -> setRange(-800, 800);
	initVelY = new QSlider();
	initVelY -> setRange(-800, 800);
	initVelZ = new QSlider();
	initVelZ -> setRange(-800, 800);
	initVelX -> setOrientation(Qt::Horizontal);
	initVelY -> setOrientation(Qt::Horizontal);
	initVelZ -> setOrientation(Qt::Horizontal);
	vxText = new QLabel("VX:");
	vyText = new QLabel("VY:");
	vzText = new QLabel("VZ:");
	initVelReset = new QPushButton("Reset");

	//Create and arrange layouts
	radiusLayout = new QGridLayout();
	radiusLayout -> addWidget(radiusText, 0, 0);
	radiusLayout -> addWidget(radius, 0, 1);
	radiusLayout -> addWidget(radiusReset, 0, 2);

	posLayout = new QGridLayout();
	posLayout -> addWidget(xText, 0, 0);
	posLayout -> addWidget(yText, 1, 0);
	posLayout -> addWidget(zText, 2, 0);
	posLayout -> addWidget(initPosX, 0, 1);
	posLayout -> addWidget(initPosY, 1, 1);
	posLayout -> addWidget(initPosZ, 2, 1);
	posLayout -> addWidget(initPosReset, 0, 2);

	velLayout = new QGridLayout();
	velLayout -> addWidget(vxText, 0, 0);
	velLayout -> addWidget(vyText, 1, 0);
	velLayout -> addWidget(vzText, 2, 0);
	velLayout -> addWidget(initVelX, 0, 1);
	velLayout -> addWidget(initVelY, 1, 1);
	velLayout -> addWidget(initVelZ, 2, 1);
	velLayout -> addWidget(initVelReset, 0, 2);

	mainLayout = new QVBoxLayout();
	mainLayout -> addLayout(radiusLayout);
	mainLayout -> addLayout(posLayout);
	mainLayout -> addLayout(velLayout);

	//Set main layout
	this -> setLayout(mainLayout);

	//Make connections
	connect(this -> radiusReset, SIGNAL(clicked()), this, SLOT(resetRadiusSlider()));

	connect(this -> initPosReset, SIGNAL(clicked()), this, SLOT(resetPosSliders()));

	connect(this -> initVelReset, SIGNAL(clicked()), this, SLOT(resetVelSliders()));
}

BallSettingsWidget::~BallSettingsWidget()
{
	delete radius;
	delete radiusReset;
	delete radiusLayout;

	delete initPosX;
	delete initPosY;
	delete initPosZ;
	delete initPosReset;
	delete posLayout;

	delete initVelX;
	delete initVelY;
	delete initVelZ;
	delete initVelReset;
	delete velLayout;

	delete mainLayout;
}

void BallSettingsWidget::setRadiusSlider(float val)
{
	std::cout << "setting radius slider: " << val << std::endl;
	radius -> setValue((int)(val * 100));
}

void BallSettingsWidget::setXSlider(float x)
{
	initPosX -> setValue((int)(x * 100));
}

void BallSettingsWidget::setYSlider(float y)
{
	initPosY -> setValue((int)(y * 100));
}

void BallSettingsWidget::setZSlider(float z)
{
	initPosZ -> setValue((int)(z * 100));
}

void BallSettingsWidget::setVXSlider(float vx)
{
	initVelX -> setValue((int)(vx * 100));
}

void BallSettingsWidget::setVYSlider(float vy)
{
	initVelY -> setValue((int)(vy * 100));
}

void BallSettingsWidget::setVZSlider(float vz)
{
	initVelZ -> setValue((int)(vz * 100));
}

void BallSettingsWidget::resetRadiusSlider()
{
	setRadiusSlider(1.0);
}

void BallSettingsWidget::resetPosSliders()
{
	setXSlider(0.0);
	setYSlider(0.0);
	setZSlider(0.0);
}

void BallSettingsWidget::resetVelSliders()
{
	setVXSlider(0.0);
	setVYSlider(0.0);
	setVZSlider(0.0);
}