#pragma once
#include "Floor.hpp"
#include <vector>

class Ball
{

	public:
		Ball();
		Ball(float rad, float mass, float x, float y, float z, float vx, float vy, float vz);

		//Getters
		float getRadius() {return r;};
		float getMass() {return m;};

		float getInitPos(int i) {return pos0[i];};
		float getInitVel(int i) {return vel0[i];};
		float getPos(int i) {return pos[i];};
		float getVel(int i) {return vel[i];};

		//Setters
		void setRadius(float newR) {r = newR;};
		void setMass(float newM) {m = newM;};
		
		void setInitPos(int i, float val) {pos0[i] = val;};
		void setInitVel(int i, float val) {vel0[i] = val;};
		void setPos(int i, float val) {pos[i] = val;};
		void setVel(int i, float val) {vel[i] = val;};
		
		//Physics update method
		void update(float dt, Floor f, std::vector<Ball*> balls);

		//Ball draw method
		void draw(float red, float green, float blue);

		//Reset the ball's physical parameters
		void reset();
		
	private:

		//Radius
		float r;

		//Mass
		float m;

		//Physics variables
		float pos0[3];	//Initial position 
		float vel0[3];	//Initial velocity

		float pos[3];
		float vel[3];
		float acc[3];

		void floorCollision(Floor f);
		void ballCollision(std::vector<Ball*> balls);
};