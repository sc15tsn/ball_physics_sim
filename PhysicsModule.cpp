#include "PhysicsModule.hpp"
#include <iostream>

PhysicsModule::PhysicsModule()
{
	fps = 1;
	floor = new Floor(14, 0.0);
}

PhysicsModule::~PhysicsModule()
{
	for(int i = 0; i < objects.size(); i++)
	{
		delete objects[i];
	}

	delete floor;
}

void PhysicsModule::update()
{
	//Update all objects
	for(int i = 0; i < objects.size(); i++)
	{
		//Copy ball vector and remove this ball from it (to prevent collisions with self)
		std::vector<Ball*> others = objects;
		others.erase(others.begin() + i);

		objects[i]->update(getUpdateRate(), *floor, others);
	}
}

void PhysicsModule::reset()
{
	for(int i = 0; i < objects.size(); i++)
	{
		objects[i]->reset();
	}
}

void PhysicsModule::addBall(Ball* b)
{
	objects.push_back(b);
}

void PhysicsModule::deleteBall(Ball* b)
{
	bool found = false;
	int pos = 0;

	for(int i = 0; i < objects.size(); i++)
	{
		pos = i;

		if(objects[i] == b)
		{
			found = true;
			break;
		}
	}

	if(found)
	{
		objects.erase(objects.begin() + pos);
	}
}