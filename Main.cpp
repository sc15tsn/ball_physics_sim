#include <QApplication>
#include <QMainWindow>
#include <GL/glut.h>
#include "MainWidget.hpp"
#include "BallMainWindow.hpp"

int main(int argc, char** argv)
{
	//Create app and window
	QApplication app(argc, argv);
	glutInit(&argc, argv);
	BallMainWindow window;

	//Attach widget to window
	MainWidget mainWidget(NULL);
	window.setCentralWidget(&mainWidget);

	//Display window and enter event loop
	window.show();
	return app.exec();
}