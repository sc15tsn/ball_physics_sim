#include "WorldSettingsWidget.hpp"

WorldSettingsWidget::WorldSettingsWidget(QWidget* parent): QFrame(parent)
{

	//Create elements
	simText = new QLabel("Simulation Properties");

	collisionText = new QLabel("C:");
	collision = new QSlider();
	collision -> setOrientation(Qt::Horizontal);
	collision -> setRange(0, 100);
	collisionReset = new QPushButton("Reset");

	fpsText = new QLabel("FPS:");
	fps = new QSlider();
	fps -> setOrientation(Qt::Horizontal);
	fps -> setRange(1, 60);
	fps -> setValue(30);
	fpsReset = new QPushButton("Reset");

	floorText = new QLabel("Floor Properties");

	sizeText = new QLabel("Size:");
	size = new QSlider();
	size -> setOrientation(Qt::Horizontal);
	size -> setRange(1, 14);
	size -> setValue(7);
	sizeReset = new QPushButton("Reset");

	heightText = new QLabel("Height:");
	height = new QSlider();
	height -> setOrientation(Qt::Horizontal);
	height -> setRange(0, 1000);
	height -> setValue(0);
	heightReset = new QPushButton("Reset");

	//Arrange elements in layouts
	simLayout = new QGridLayout();
	simLayout -> addWidget(simText, 0, 0, 1, 2);
	simLayout -> addWidget(collisionText, 1, 0);
	simLayout -> addWidget(collision, 1, 1);
	simLayout -> addWidget(collisionReset, 1, 2);
	simLayout -> addWidget(fpsText, 2, 0);
	simLayout -> addWidget(fps, 2, 1);
	simLayout -> addWidget(fpsReset, 2, 2);

	floorLayout = new QGridLayout();
	floorLayout -> addWidget(floorText, 0, 0, 1, 2);
	floorLayout -> addWidget(sizeText, 1, 0);
	floorLayout -> addWidget(size, 1, 1);
	floorLayout -> addWidget(sizeReset, 1, 2);
	floorLayout -> addWidget(heightText, 2, 0);
	floorLayout -> addWidget(height, 2, 1);
	floorLayout -> addWidget(heightReset, 2, 2);

	mainLayout = new QVBoxLayout();
	mainLayout -> addLayout(simLayout);
	mainLayout -> addLayout(floorLayout);

	//Set main layout
	this -> setLayout(mainLayout);

	//Make connections
	connect(this -> fpsReset, SIGNAL(clicked()), this, SLOT(resetFps()));
	connect(this -> heightReset, SIGNAL(clicked()), this, SLOT(resetHeight()));
	connect(this -> sizeReset, SIGNAL(clicked()), this, SLOT(resetSize()));

}

WorldSettingsWidget::~WorldSettingsWidget()
{
	delete simText;
	delete floorText;
	delete collisionText;
	delete collision;
	delete collisionReset;
	delete fpsText;
	delete fps;
	delete fpsReset;
	delete sizeText;
	delete size;
	delete sizeReset;
	delete heightText;
	delete height;
	delete heightReset;
	delete simLayout;
	delete floorLayout;
	delete mainLayout;
}

void WorldSettingsWidget::resetFps()
{
	fps -> setValue(30);
}

void WorldSettingsWidget::resetSize()
{
	size -> setValue(7);
}

void WorldSettingsWidget::resetHeight()
{
	height -> setValue(0);
}