#include <GL/glu.h>
#include <GL/glut.h>
#include "Floor.hpp"


Floor::Floor(int s, float h)
{
	size = s;
	height = h;
}

void Floor::draw()
{
	int x;
	int z;
	int squaresDrawn = 0;
	float normal[3] = {0.0, 1.0, 0.0};

	float whiteMaterial[4] = {1.0, 1.0, 1.0, 1.0};
	float blackMaterial[4] = {0.0, 0.0, 0.0, 0.0};
	float shininess = 0.4; 


	for(x = -(size / 2); x < (size / 2); x++)
	{
		for(z = -(size / 2); z < (size / 2); z++)
		{
			//Swap colour ordering every column to avoid rows matching colours
			if(x % 2 == 0)
			{	
				//Alternate between black and white to avoid colomn colours matching
				if(squaresDrawn % 2 == 0)
				{
					glMaterialfv(GL_FRONT, GL_AMBIENT, blackMaterial);
				  	glMaterialfv(GL_FRONT, GL_DIFFUSE, blackMaterial);
				  	glMaterialfv(GL_FRONT, GL_SPECULAR, blackMaterial);
				 	glMaterialf(GL_FRONT, GL_SHININESS, shininess * 128);
				}

				else
				{
					glMaterialfv(GL_FRONT, GL_AMBIENT, whiteMaterial);
				  	glMaterialfv(GL_FRONT, GL_DIFFUSE, whiteMaterial);
				  	glMaterialfv(GL_FRONT, GL_SPECULAR, whiteMaterial);
				 	glMaterialf(GL_FRONT, GL_SHININESS, shininess * 128);
				}
			}

			else
			{
				if(squaresDrawn % 2 == 0)
				{
					glMaterialfv(GL_FRONT, GL_AMBIENT, whiteMaterial);
				  	glMaterialfv(GL_FRONT, GL_DIFFUSE, whiteMaterial);
				  	glMaterialfv(GL_FRONT, GL_SPECULAR, whiteMaterial);
				 	glMaterialf(GL_FRONT, GL_SHININESS, shininess * 128);
				}

				else
				{
					glMaterialfv(GL_FRONT, GL_AMBIENT, blackMaterial);
				  	glMaterialfv(GL_FRONT, GL_DIFFUSE, blackMaterial);
				  	glMaterialfv(GL_FRONT, GL_SPECULAR, blackMaterial);
				 	glMaterialf(GL_FRONT, GL_SHININESS, shininess * 128);
				}
			}

			glNormal3fv(normal);
			glBegin(GL_POLYGON);
				glVertex3f(x, height, z);
				glVertex3f(x, height, z + 1);
				glVertex3f(x + 1, height, z + 1);
				glVertex3f(x + 1, height, z);
			glEnd();

			squaresDrawn++;

		}
	}
}