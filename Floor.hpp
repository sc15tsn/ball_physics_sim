#pragma once

class Floor
{
	public:
		Floor(int s, float h);

		int getSize() {return size;};
		float getHeight() {return height;};

		void setSize(int s) {size = s;};
		void setHeight(float h) {height = h;};

		void draw();

	private:
		
		int size;
		float height;

};