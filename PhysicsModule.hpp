#pragma once

#include <chrono>
#include <vector>
#include "Ball.hpp"
#include <QObject>
#include "Floor.hpp"

class PhysicsModule
{

	public:

		PhysicsModule();

		~PhysicsModule();

		int getFps() {return fps;};
		void setFps(int given) {fps = given;};
		
		Floor* getFloor() {return floor;};

		//Add ball
		void addBall(Ball* b);

		void deleteBall(Ball* b);
		
		//Get reference to ball vector
		std::vector<Ball*>& getBalls() {return objects;};

		float getUpdateRate() {return (1 / (float) fps);};

		int getNumOfObjects() {return objects.size();};

		Ball* getBall(int index) {return objects[index];};

		//Run physics step
		void update();

		//Reset physical system
		void reset();


	public slots:

		void setActiveRadius(int radius);

	signals:

		void publishActiveRadius(float r);

	private:

		//Frames per second
		int fps;

		//List of objects to apply physics to
		std::vector<Ball*> objects;

		//World floor
		Floor* floor;
};