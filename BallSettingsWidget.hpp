#include <QWidget>
#include <QFrame>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QSlider>
#include <QPushButton>
#include <QColorDialog>
#include <QLabel>

#include "Ball.hpp"

class BallSettingsWidget: public QFrame
{

	Q_OBJECT

	public:

		BallSettingsWidget(QWidget* parent);
		~BallSettingsWidget();

		QSlider* getRadiusSlider() {return radius;};
		QSlider* getMassSlider() {return mass;};

		QSlider* getXSlider() {return initPosX;};
		QSlider* getYSlider() {return initPosY;};
		QSlider* getZSlider() {return initPosZ;};

		QSlider* getVXSlider() {return initVelX;};
		QSlider* getVYSlider() {return initVelY;};
		QSlider* getVZSlider() {return initVelZ;};

		QPushButton* getRadiusReset() {return radiusReset;};
		QPushButton* getMassReset() {return massReset;};
		QPushButton* getPosReset() {return initPosReset;};
		QPushButton* getVelReset() {return initVelReset;};

	public slots:

		void setRadiusSlider(float val);
		void setMassSlider(float val);

		void setXSlider(float x);
		void setYSlider(float y);
		void setZSlider(float z);

		void setVXSlider(float vx);
		void setVYSlider(float vy);
		void setVZSlider(float vz);

		void resetRadiusSlider();
		void resetMassSlider();
		
		void resetPosSliders();

		void resetVelSliders();

		void disable() {setEnabled(false);};
		void enable() {setEnabled(true);};

	private:

		//Elements
		QLabel* generalText;

		QLabel* radiusText;
		QSlider* radius;
		QPushButton* radiusReset;

		QLabel* massText;
		QSlider* mass;
		QPushButton* massReset;
		
		QLabel* posText;

		QLabel* xText;
		QLabel* yText;
		QLabel* zText;
		QSlider* initPosX;
		QSlider* initPosY;
		QSlider* initPosZ;
		QPushButton* initPosReset;

		QLabel* velText;

		QLabel* vxText;
		QLabel* vyText;
		QLabel* vzText;
		QSlider* initVelX;
		QSlider* initVelY;
		QSlider* initVelZ;
		QPushButton* initVelReset;

		//Layouts
		QVBoxLayout* mainLayout;
		QGridLayout* propLayout;
		QGridLayout* posLayout;
		QGridLayout* velLayout;
		QGridLayout* colourLayout;
};