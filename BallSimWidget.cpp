#include <GL/glut.h>
#include "BallSimWidget.hpp"
#include <string>
#include <iostream>

BallSimWidget::BallSimWidget(QWidget* parent): QGLWidget(parent)
{
	setFocusPolicy(Qt::StrongFocus);

	//Default world params
	worldRotX = 0;
	worldRotX = 0;
	worldRotY = 0;

	physics = new PhysicsModule();

	//Set refresh rate of physics sim
	timer = new QTimer();

	//Connect timer to physics update
	connect(timer, SIGNAL(timeout()), this, SLOT(updatePhysics()));

}

BallSimWidget::~BallSimWidget()
{
	delete physics;
	delete timer;
}

void BallSimWidget::initializeGL()
{

	disableActiveBall();
	setFps(30);

	//Set background colour to gray
	glClearColor(bgColourR, bgColourG, bgColourB, 0.0);

	//Set projection mode
	glMatrixMode(GL_PROJECTION);
		
		glLoadIdentity();
		glFrustum(-1.536, 1.536, -1.080, 1.080, 1, 100);

	glMatrixMode(GL_MODELVIEW);
		
		glEnable(GL_DEPTH_TEST);
		glLoadIdentity();

		//Set up lighting
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		GLfloat lightPos[] = {lightPosX, lightPosY, lightPosZ, 1};
		GLfloat specular[] = {0.2, 0.2, 0.2};
		glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
		glLightfv(GL_LIGHT0, GL_SPECULAR, specular);

		//Set up smooth shading
		glShadeModel(GL_SMOOTH);
}

void BallSimWidget::resizeGL(int width, int height)
{
	glViewport(0, 0, width, height);
}

void BallSimWidget::paintGL()
{	

	//Clear screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	
		glLoadIdentity();

		//Setup camera
		gluLookAt(camPosX, camPosY, camPosZ, 0, 0, 0, 0, 1, 0);

		
		glRotatef(worldRotX, 1, 0, 0);
		glRotatef(worldRotY, 0, 1, 0);

		//Draw the floor
		physics -> getFloor() -> draw();

		float red, green, blue = 0.0;

		//Draw the balls
		for(Ball* b: physics->getBalls())
		{	
			if(b == active) {red = 0.9; green = 0.0; blue = 0.0;}
			else {red = 0.2; green = 0.2; blue = 0.2;};
			b->draw(red, green, blue);
		}

	//Draw
	glFlush();
}

void BallSimWidget::mousePressEvent(QMouseEvent* event)
{
	//Record mouse position
	mousePos = event -> pos();
}

void BallSimWidget::mouseMoveEvent(QMouseEvent* event)
{
	//Get difference vector from previous mouse position
	QPointF mouseMovement = event -> pos() - mousePos;

	//Decompose into vertical and horizontal components
	float mouseDx = mouseMovement.x();
	float mouseDy = mouseMovement.y();

	//Turn into x and y angles and apply to world rotation
	if((worldRotX + (mouseDy * rotateFactor) <= 70) && (worldRotX + (mouseDy * rotateFactor) >= 0))
	{
		worldRotX += mouseDy * rotateFactor;
	}

	worldRotY += mouseDx * rotateFactor;

	//Wrap around if X rotation is below 0 or above 360
	if(worldRotX < 0)
	{
		worldRotX += 360;
	}

	if(worldRotX >= 360)
	{
		worldRotX -= 360;
	}

	//Update mouse position
	mousePos = event -> pos();

	//Redraw scene
	update();
}

void BallSimWidget::mouseReleaseEvent(QMouseEvent* event)
{
	//Stop tracking mouse position
	mousePos = *(new QPointF(0, 0));
}

void BallSimWidget::wheelEvent(QWheelEvent* event)
{
	//Get the distance the wheel was moved
	int mouseDistance = event -> delta();
	
	//Apply to world scale
	if(camPosZ + mouseDistance * translateZFactor > 5)
	{
		camPosZ += mouseDistance * translateZFactor;
	}

	//Redraw scene
	update();
}

void BallSimWidget::keyReleaseEvent(QKeyEvent* event)
{
	if(event -> key() == Qt::Key_Right)
	{
		activeIndex++;

		if(activeIndex < (physics -> getNumOfObjects()))
		{
			setActiveBall(physics -> getBall(activeIndex));
		}

		else
		{
			disableActiveBall();
		}
	}

	if(event -> key() == Qt::Key_Left)
	{
		activeIndex--;

		if(activeIndex < -1)
		{
			activeIndex = physics -> getNumOfObjects() - 1;
		}

		if(activeIndex > -1)
		{
			setActiveBall(physics -> getBall(activeIndex));
		}

		else
		{
			disableActiveBall();
		}
	}

	update();
	
}

void BallSimWidget::resetWorld()
{
	//Set world params back to default values
	worldRotX = worldRotY = 0;

	//Redraw scene
	update();
}

void BallSimWidget::playSimulation()
{
	timer->start();
}

void BallSimWidget::pauseSimulation()
{
	timer->stop();
}

void BallSimWidget::resetSimulation()
{
	timer->stop();
	physics->reset();
	update();
}

void BallSimWidget::updatePhysics()
{
	//Update physics and redraw
	physics->update();
	update();
}

void BallSimWidget::setActiveRadius(int r)
{
	active -> setRadius((float) r / 100);
	update();
}

void BallSimWidget::setActiveMass(int m)
{
	active -> setMass((float) m / 100);
	update();
}

void BallSimWidget::setActiveX(int x)
{
	active -> setInitPos(0, (float) x / 100);
	active -> setPos(0, (float) x / 100);
	update();
}

void BallSimWidget::setActiveY(int y)
{
	active -> setInitPos(1, (float) y / 100);
	active -> setPos(1, (float) y / 100);
	update();
}

void BallSimWidget::setActiveZ(int z)
{
	active -> setInitPos(2, (float) z / 100);
	active -> setPos(2, (float) z / 100);
	update();
}

void BallSimWidget::setActiveVX(int vx)
{
	active -> setInitVel(0, (float) vx / 100);
	active -> setVel(0, (float) vx / 100);
	update();
}

void BallSimWidget::setActiveVY(int vy)
{
	active -> setInitVel(1, (float) vy / 100);
	active -> setVel(1, (float) vy / 100);
	update();
}

void BallSimWidget::setActiveVZ(int vz)
{
	active -> setInitVel(2, (float) vz / 100);
	active -> setVel(2, (float) vz / 100);
	update();
}

void BallSimWidget::setFps(int fps)
{
	physics -> setFps(fps);
	timer -> setInterval(physics->getUpdateRate() * 1000);
}

void BallSimWidget::setActiveBall(Ball* b)
{
	active = b;

	emit publishActiveRadius(b -> getRadius());
	emit publishActiveMass(b -> getMass());

	emit publishActiveX(b -> getInitPos(0));
	emit publishActiveY(b -> getInitPos(1));
	emit publishActiveZ(b -> getInitPos(2));

	emit publishActiveVX(b -> getInitVel(0));
	emit publishActiveVY(b -> getInitVel(1));
	emit publishActiveVZ(b -> getInitVel(2));

	emit newActiveBall();
}

void BallSimWidget::addBall()
{
	Ball* b = new Ball(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	physics -> addBall(b);
	activeIndex = physics -> getNumOfObjects() - 1;
	setActiveBall(b);
	update();
}

void BallSimWidget::disableActiveBall()
{
	activeIndex = -1;
	active = NULL;

	emit noActiveBall();
}

void BallSimWidget::deleteActiveBall()
{
	physics -> deleteBall(active);
	disableActiveBall();
	update();
}

void BallSimWidget::setFloorSize(int size)
{
	physics -> getFloor() -> setSize(size * 2);
	update();
}

void BallSimWidget::setFloorHeight(int h)
{
	physics -> getFloor() -> setHeight(-(float) h / 100);
	update();
}