#include <QMainWindow>
#include <QResizeEvent>

class BallMainWindow: public QMainWindow
{
	public:
		BallMainWindow();

	private:
		virtual void resizeEvent(QResizeEvent* e);
};