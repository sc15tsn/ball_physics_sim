#include <QWidget>
#include <QHBoxLayout>
#include "DisplayWidget.hpp"
#include "BallSettingsWidget.hpp"
#include "WorldSettingsWidget.hpp"

class MainWidget: public QWidget
{
	Q_OBJECT

	public:
		MainWidget(QWidget* parent);
		~MainWidget();

		BallSettingsWidget* getBallSettings() {return bSettings;};
		WorldSettingsWidget* getWorldSettings() {return wSettings;};

	private:
		DisplayWidget* display;
		BallSettingsWidget* bSettings;
		WorldSettingsWidget* wSettings;

		QHBoxLayout* mainLayout;
		QVBoxLayout* settingsLayout;
};